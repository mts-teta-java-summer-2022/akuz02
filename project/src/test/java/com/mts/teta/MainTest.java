package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.mts.teta.localstorage.LocalRepository;
import com.mts.teta.model.Message;
import com.mts.teta.service.EnrichmentService;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MainTest {

    @Test
    void sanityTest() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Успешное обогащение сообщения")
    public void testSuccessEnrichment() throws InterruptedException {
        int numberOfThreads = 100;
        LocalRepository dbLocalMessageResponse = new LocalRepository();
        LocalRepository dbLocalMessage = new LocalRepository();
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        EnrichmentService enrichmentService = new EnrichmentService(dbLocalMessageResponse, dbLocalMessage);
        Message noEnrichmentTestMessage = new Message("{\"action\": \"button_click\",\"page\": \"book_card\",\"msisdn\": \"88005553535\"}", Message.EnrichmentType.MSISDN);
        for (int i = 0; i < numberOfThreads; i++) {
            int finalI = i;
            service.execute(() -> {
                Message enrichmentTestMessage = new Message("{\"action\": \"button_click\",\"page\": \"book_card\",\"msisdn\": \"88005553535" + finalI + "\",\"enrichment\": {\"firstName\": \"Vitya\",\"lastName\": \"PUPKIN\"}}", Message.EnrichmentType.MSISDN);
                enrichmentService.enrich(noEnrichmentTestMessage);
                enrichmentService.enrich(enrichmentTestMessage);
                latch.countDown();
            });
        }
        latch.await();

        Assertions.assertAll(
                () -> assertEquals(numberOfThreads, dbLocalMessageResponse.getSynchronizedList().size()),
                () -> assertEquals(numberOfThreads, dbLocalMessage.getSynchronizedList().size())
        );

      dbLocalMessageResponse.getSynchronizedList().forEach((context) -> {
          try {
              JSONAssert.assertEquals("{enrichment={\n" +
                      "  \"firstName\" : \"Иван\",\n" +
                      "  \"lastName\" : \"Курков\"\n" +
                      "}, action=button_click, page=book_card, msisdn=88005553535}", context, true);
          } catch (JSONException e) {
              assertEquals(0,1);
          }
        });

        dbLocalMessage.getSynchronizedList().forEach((context) -> { //в списке notEnrichedMessages только НЕ успешно обработанные
            try {
                JSONAssert.assertNotEquals("{enrichment={\n" +
                        "  \"firstName\" : \"Иван\",\n" +
                        "  \"lastName\" : \"Курков\"\n" +
                        "}, action=button_click, page=book_card, msisdn=88005553535}", context, true);
            } catch (JSONException e) {
                assertEquals(0,1);
            }
        });
    }

}