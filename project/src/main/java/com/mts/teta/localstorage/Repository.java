package com.mts.teta.localstorage;

public interface Repository {
    boolean save(String element);
    boolean update(String element);
}
