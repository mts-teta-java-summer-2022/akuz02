package com.mts.teta.localstorage;

import java.util.ArrayList;
import java.util.List;

public class LocalRepository implements Repository {

    List<String> synchronizedList;

    public LocalRepository() {
        this.synchronizedList = new ArrayList<>();
    }

    public LocalRepository(List<String> synchronizedList) {
        this.synchronizedList = synchronizedList;
    }

    public List<String> getSynchronizedList() {
        return synchronizedList;
    }

    @Override
    public boolean save(String element) {
        synchronized (synchronizedList) {
            return synchronizedList.add(element);
        }
    }

    @Override
    public boolean update(String element) {
        synchronized (synchronizedList) {
            boolean absent = !synchronizedList.contains(element);
            if (absent)
                synchronizedList.add(element);
            return absent;
        }
    }
}
