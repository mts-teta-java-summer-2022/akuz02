package com.mts.teta.localstorage;

import com.mts.teta.model.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DbLocalUser {

    private static ConcurrentHashMap<String, User> users = new ConcurrentHashMap<String, User>() {{
        put("88005553535", new User("Иван", "Курков"));
        put("18005553535", new User("Пётр", "Иванов"));
        put("28005553535", new User("Фёдор", "Зверев"));
    }};


    public static Map<String, User> getAllUsers() {
        return users;
    }

}
