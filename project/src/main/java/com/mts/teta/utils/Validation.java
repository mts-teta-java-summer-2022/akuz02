package com.mts.teta.utils;

public interface Validation {
    boolean isValid(String jsonString);
}
