package com.mts.teta.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mts.teta.localstorage.DbLocalUser;
import com.mts.teta.localstorage.LocalRepository;
import com.mts.teta.model.Message;
import com.mts.teta.model.MessageResult;
import com.mts.teta.model.User;
import com.mts.teta.utils.JsonValidation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public class EnrichmentService implements Enrichment {
    LocalRepository dbLocalMessageResponse;
    LocalRepository dbLocalMessage;

    public EnrichmentService(LocalRepository dbLocalMessageResponse, LocalRepository dbLocalMessage) {
        this.dbLocalMessageResponse = dbLocalMessageResponse;
        this.dbLocalMessage = dbLocalMessage;
    }

    public String enrich(Message message) {
        MessageResult messageResult = new MessageResult(message.getContent(), Message.EnrichmentType.MSISDN);
        final Map<String, User> users = DbLocalUser.getAllUsers();
        if (new JsonValidation().isValid((message.getContent()))) {
            try {
                if (new EnrichmentWorker().result(message, messageResult, users) != null) {
                    dbLocalMessageResponse.save(new EnrichmentWorker().result(message, messageResult, users).toString());
                    return new EnrichmentWorker().result(message, messageResult, users).toString();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dbLocalMessage.save(message.getContent());
        return messageResult.toString();
    }
}