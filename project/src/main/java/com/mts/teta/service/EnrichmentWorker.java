package com.mts.teta.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mts.teta.model.Message;
import com.mts.teta.model.MessageResult;
import com.mts.teta.model.User;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public class EnrichmentWorker {
    private ObjectMapper objectMapper;
    private TypeReference<HashMap<String, Object>> typeRef;

    public EnrichmentWorker() {
        this.objectMapper = new ObjectMapper();
        this.typeRef = new TypeReference<HashMap<String, Object>>() {
        };
    }

    public Map<String, Object> result(Message message, MessageResult messageResult, Map<String, User> users) throws IOException {
        Map<String, Object> messageContentJSON = objectMapper.readValue(message.getContent(), typeRef);
        if (messageContentJSON.containsKey(Message.EnrichmentType.MSISDN.name().toLowerCase(Locale.ROOT))) {
            Map<String, Object> resultJson = setEnrichment(messageResult, users, messageContentJSON);
            return resultJson;
        }
        return null;
    }

    private Map<String, Object> setEnrichment(MessageResult messageResult, Map<String, User> users, Map<String, Object> messageContentJSON) throws IOException {
        Optional<String> MSISDN = Optional.ofNullable(messageContentJSON.get(Message.EnrichmentType.MSISDN.name().toLowerCase(Locale.ROOT)).toString());
        User user = getUser(users, MSISDN);
        Map<String, Object> resultJson = objectMapper.readValue(messageResult.getContent(), typeRef);
        ObjectWriter ow = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writer().withDefaultPrettyPrinter();
        if ((user.getFirstName() != null && user.getLastName() != null)) {
            resultJson.put("enrichment", ow.writeValueAsString(user));
            return resultJson;
        }
        return null;
    }

    private User getUser(Map<String, User> users, Optional<String> MSISDN) {
        Optional<User> optionalUser = Optional.of(users.getOrDefault(MSISDN.isPresent() ? MSISDN.get() : "", new User())); // Поиск юзера
        return optionalUser.isPresent() ? optionalUser.get() : new User();
    }
}
