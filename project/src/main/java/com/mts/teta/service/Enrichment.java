package com.mts.teta.service;

import com.mts.teta.model.Message;

public interface Enrichment {
    String enrich(Message message);
}
