package com.mts.teta.model;

public class Message {
    private String content;
    private EnrichmentType enrichmentType;

    public String getContent() {
        return content;
    }

    public enum EnrichmentType {
        MSISDN;
    }

    public Message(String content, EnrichmentType enrichmentType) {
        this.content = content;
        this.enrichmentType = enrichmentType;
    }
}
