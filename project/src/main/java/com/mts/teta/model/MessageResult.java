package com.mts.teta.model;

public class MessageResult extends Message {

    private String firstName;
    private String lastName;
    private String enrichment;

    public MessageResult(String content, Message.EnrichmentType enrichmentType) {
        super(content, enrichmentType);
    }
}
