package com.mts.teta.task2.service;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.model.request.CourseRequest;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CourseService {
    Course createCourse(CourseRequest request);
    Course updateCourse(Integer id, CourseRequest request);
    boolean deleteCourse(Integer id);
    List<Course> getCourseByPrefix(String prefix);
    Optional<Course> getCourseByAuthorAndTitle(String title, String author);
    Course assignUser(Long courseId, Long id);
}