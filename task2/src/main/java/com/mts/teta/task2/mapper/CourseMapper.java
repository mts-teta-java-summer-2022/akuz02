package com.mts.teta.task2.mapper;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.model.request.CourseRequest;
import com.mts.teta.task2.model.response.CourseResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseMapper {
    CourseResponse toDto(Course source);
    Course toEntity(CourseRequest source);
}
