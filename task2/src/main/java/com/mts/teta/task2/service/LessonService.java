package com.mts.teta.task2.service;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.response.LessonResponse;

import javax.transaction.Transactional;
import java.util.List;

public interface LessonService {
    LessonResponse createLesson(LessonRequest lesson);
    LessonResponse deletedLesson(Long lessonId);
    LessonResponse updateLesson(LessonRequest lesson);
    List<LessonResponse> getLessonsByCourseId(Long courseId);
}
