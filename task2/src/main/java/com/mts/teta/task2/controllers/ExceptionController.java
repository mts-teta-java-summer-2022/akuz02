package com.mts.teta.task2.controllers;

import com.mts.teta.task2.model.response.exception.ExceptionResponse;
import com.mts.teta.task2.model.response.exception.NotFoundResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.persistence.EntityNotFoundException;

import java.time.OffsetDateTime;
import java.util.NoSuchElementException;

import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;

@RestControllerAdvice
public class ExceptionController {
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    private ResponseEntity<NotFoundResponse> notFound(EntityNotFoundException ex) {
        String dateOccurred = OffsetDateTime.now().format(ISO_ZONED_DATE_TIME);
        return new ResponseEntity<>(new NotFoundResponse(ex.getMessage(), dateOccurred), HttpStatus.OK);
    }

    @ExceptionHandler(NoSuchElementException.class)
    private NotFoundResponse notSuch(NoSuchElementException ex) {
        String dateOccurred = OffsetDateTime.now().format(ISO_ZONED_DATE_TIME);
        return new NotFoundResponse(ex.getMessage(), dateOccurred);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    private ExceptionResponse error(RuntimeException ex) {
        return new ExceptionResponse(ex.getMessage());
    }
}
