package com.mts.teta.task2.controllers;

import com.mts.teta.task2.domain.EnumRole;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "admin-docs")
@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/api/v1/admin")
@RequiredArgsConstructor
public class AdminPanelController {
    @PostMapping("/login")
    public ResponseEntity<String> login() {
        return ResponseEntity.ok("OK");
    }
}
