package com.mts.teta.task2.service;

import com.mts.teta.task2.domain.User;
import com.mts.teta.task2.mapper.CourseMapper;
import com.mts.teta.task2.model.request.CourseRequest;
import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.model.response.CourseResponse;
import com.mts.teta.task2.repository.CourseRepository;
import com.mts.teta.task2.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final UserRepository userRepository;
    private final CourseMapper mapper;

    /**
     * //     * Процедура создания курса
     * //     *
     * //     * @param request - json модель
     * //     * @return Course
     * //
     */
    @Override
    public Course createCourse(@NotNull CourseRequest request) {
        Optional<Course> course = courseRepository.findByTitleAndAuthor(request.getTitle(), request.getAuthor());
        if (course.isEmpty()) {
            return courseRepository.save(mapper.toEntity(request));
        }
        return null;
    }

    /**
     * //     * Процедура обновления курса
     * //     *
     * //     * @param id - идентификатор курса
     * //     * @param request - json модель
     * //     * @return boolean, true - если операция успешна, иначе false
     * //
     */
    @Override
    public Course updateCourse(Integer id, CourseRequest request) {
        Optional<Course> course = courseRepository.findById(id);
        course.ifPresent(courseUpdated -> {
            Course courseNewData = mapper.toEntity(request);
            courseNewData.setId(courseUpdated.getId());
            courseRepository.save(courseNewData);
        });
        return course.get();
    }

    /**
     * //     * Процедура удаления курса
     * //     *
     * //     * @param id - идентификатор курса
     * //     * @return boolean, true - если операция успешна, иначе false
     * //
     */
    @Override
    public boolean deleteCourse(Integer id) {
        boolean result = false;
        if (courseRepository.findById(id).isPresent()) {
            courseRepository.deleteById(id);
            result = true;
        }
        return result;
    }

    /**
     * //     * Процедура получения курса по префиксу
     * //     *
     * //     * @param prefix - искомый префикс
     * //     * @return List<Course>, список найденных курсов с указанным префиксом
     * //
     */
    @Override
    public List<Course> getCourseByPrefix(String prefix) {
        List<Course> course = courseRepository.findByTitleStartingWith(prefix);
        if (course.isEmpty()) {
            return new ArrayList<>();
        } else {
            return course;
        }
    }

    /**
     * //     * Процедура получения курса по автору и названию курса
     * //     *
     * //     * @param title - название курса
     * //     * @param author - автор курса
     * //     * @return List<Course>, список найденных курсов с указанным префиксом
     * //
     */
    @Override
    public Optional<Course> getCourseByAuthorAndTitle(String title, String author) {
        return courseRepository.findByTitleAndAuthor(title, author);
    }

    @Override
    public Course assignUser(Long courseId, Long id) {
        User user = userRepository.getById(id);
        Course course = courseRepository.getById(courseId.intValue());
        course.getUsers().add(user);
        user.getCourses().add(course);
        return courseRepository.save(course);
    }

}
