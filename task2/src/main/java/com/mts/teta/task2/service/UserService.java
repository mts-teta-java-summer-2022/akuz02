package com.mts.teta.task2.service;

import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.domain.User;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.request.UserRequest;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.model.response.UserResponse;

public interface UserService {
    UserResponse createUser(UserRequest user);
    UserResponse deletedUser(Long userId);
}
