package com.mts.teta.task2.model.response.exception;

import lombok.Data;

@Data
public class ExceptionResponse {
    private final String massage;
}
