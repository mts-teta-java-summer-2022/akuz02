package com.mts.teta.task2.repository;

import com.mts.teta.task2.domain.Course;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface CourseRepository extends JpaRepository<Course, Integer> {
    Optional<Course> findByTitleAndAuthor(String title, String author);
    List<Course> findByTitleStartingWith(String title);
    List<Course> findByTitleLike(String title);
}
