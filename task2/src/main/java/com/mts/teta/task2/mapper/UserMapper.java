package com.mts.teta.task2.mapper;

import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.domain.User;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.request.UserRequest;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.model.response.UserResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserRequest toDto(User source);
    UserResponse toResponseDto(User source);
    User toEntity(UserRequest source);
}
