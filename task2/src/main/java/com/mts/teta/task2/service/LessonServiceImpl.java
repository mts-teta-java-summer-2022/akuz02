package com.mts.teta.task2.service;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.mapper.CourseMapper;
import com.mts.teta.task2.mapper.LessonMapper;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.repository.CourseRepository;
import com.mts.teta.task2.repository.LessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;
    private final LessonMapper mapper;

    @Override
    public LessonResponse createLesson(LessonRequest lessonRequest) {
        Optional<Course> course = courseRepository.findById(lessonRequest.getCourseId().intValue());
        Course course1 = courseRepository.getById(lessonRequest.getCourseId().intValue());
        Lesson lesson = mapper.toEntity(lessonRequest);
        lesson.setCourse(course1);
        lessonRepository.save(lesson);
        LessonResponse lessonResponse = mapper.toResponseDto(lesson);
        lessonResponse.setCourseId(course1.getId().longValue());
        return lessonResponse;
    }

    @Override
    public LessonResponse deletedLesson(Long lessonId) {
        Integer courseId = lessonRepository.findById(lessonId).get().getCourse().getId();
        Course course = courseRepository.getById(courseId);
        Lesson lesson = lessonRepository.getById(lessonId);
        course.getLessons().remove(lesson);
        courseRepository.save(course);
        return mapper.toResponseDto(lesson);
    }

    @Override
    public LessonResponse updateLesson(LessonRequest lesson) {
        Optional<Course> courseOptional = courseRepository.findById(lesson.getCourseId().intValue());
        courseOptional.ifPresentOrElse(course -> {
        Optional<Lesson> lessonOptional = lessonRepository.findById(lesson.getId());
        lessonOptional.ifPresentOrElse((lessonItem -> {
            mapper.updateLessonFromDto(lesson, lessonItem);
            lessonRepository.save(lessonItem);
        }), (() -> {throw new RuntimeException("Lesson not found");}));

        },  (() -> {throw new RuntimeException("Course not found");}));

        return mapper.toResponseDto(lessonRepository.findById(lesson.getId()).get());
    }

    @Override
    public List<LessonResponse> getLessonsByCourseId(Long courseId) {
        List<LessonResponse> lessons = courseRepository.findById(courseId.intValue())
                .get()
                .getLessons()
                .stream()
                .map(l -> new LessonResponse(l.getId(), l.getTitle(), l.getText(), l.getCourse().getId().longValue()))
                .collect(Collectors.toList());
        return lessons;
    }
}
