package com.mts.teta.task2.controllers;

import com.mts.teta.task2.mapper.CourseMapper;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.request.UserRequest;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.model.response.UserResponse;
import com.mts.teta.task2.service.LessonService;
import com.mts.teta.task2.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "user-docs")
@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @ApiOperation(value = "User creation operation")
    @PostMapping("/create")
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody UserRequest request) {
        UserResponse lesson = userService.createUser(request);
        return ResponseEntity.ok(lesson);
    }
}
