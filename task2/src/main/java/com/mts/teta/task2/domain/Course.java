package com.mts.teta.task2.domain;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;
import static org.hibernate.annotations.FetchMode.SUBSELECT;

@Getter
@Setter
@Entity
@Table(name = "courses")
@NoArgsConstructor
@AllArgsConstructor

public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Course author have to be filled")
    @Column(name = "author", nullable = false)
    private String author;

    @NotBlank(message = "Course category have to be filled")
    @Column(name = "category", nullable = false, unique = true)
    private String category;

    @NotBlank(message = "Course title have to be filled")
    @Column(name = "title", nullable = false)
    private String title;

    @OneToMany(mappedBy = "course", orphanRemoval = true,  cascade = CascadeType.ALL,
            fetch = EAGER)
    private List<Lesson> lessons;

    @ManyToMany
    private Set<User> users;

}
