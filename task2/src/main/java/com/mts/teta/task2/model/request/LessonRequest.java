package com.mts.teta.task2.model.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LessonRequest {
    private Long id;
    private String title;
    private String text;
    private Long courseId;
}