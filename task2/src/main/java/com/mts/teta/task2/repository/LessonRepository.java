package com.mts.teta.task2.repository;

import com.mts.teta.task2.domain.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface LessonRepository extends JpaRepository<Lesson, Long> {
        Optional<Lesson> findByTitle(String title);
        Optional<Lesson> findById(Long id);
}
