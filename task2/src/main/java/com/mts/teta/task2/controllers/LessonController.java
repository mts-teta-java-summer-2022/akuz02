package com.mts.teta.task2.controllers;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.mapper.CourseMapper;
import com.mts.teta.task2.mapper.LessonMapper;
import com.mts.teta.task2.model.request.CourseRequest;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.response.CourseResponse;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.service.CourseService;
import com.mts.teta.task2.service.LessonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Api(value = "lesson-docs")
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class LessonController {
    private final LessonService lessonService;
    private final LessonMapper mapper;

    @ApiOperation(value = "Course creation operation")
    @PostMapping("/lesson/create")
    public ResponseEntity<LessonResponse> createLesson(@Valid @RequestBody LessonRequest request) {
        LessonResponse lesson = lessonService.createLesson(request);
        return ResponseEntity.ok(lesson);
    }

    @ApiOperation(value = "Course creation operation")
    @GetMapping("/lesson/delete")
    public ResponseEntity<LessonResponse> deleteLesson(@RequestParam("id") Long id) {
        LessonResponse lesson = lessonService.deletedLesson(id);
        return ResponseEntity.ok(lesson);
    }

    @GetMapping("/lessons")
    public ResponseEntity<List<LessonResponse>> getLessonsByCourseId(@RequestParam("courseId") Long courseId) {
        List<LessonResponse> body = lessonService.getLessonsByCourseId(courseId);
        return new ResponseEntity<List<LessonResponse>>(body, HttpStatus.OK);
    }

    @PatchMapping("/lesson/update")
    public ResponseEntity<LessonResponse> updateLesson(@Valid @RequestBody LessonRequest request) {
        LessonResponse body = lessonService.updateLesson(request);
        return new ResponseEntity<LessonResponse>(body, HttpStatus.OK);
    }
}
