package com.mts.teta.task2.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "roles")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @Enumerated(EnumType.STRING)
    private EnumRole name;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public Role(EnumRole name) {
        this.name = name;
    }

}
