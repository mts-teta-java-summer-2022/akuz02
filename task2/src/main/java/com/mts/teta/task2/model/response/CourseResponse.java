package com.mts.teta.task2.model.response;

import com.mts.teta.task2.domain.Lesson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseResponse {
    private Integer id;
    private String author;
    private String title;
    private String category;
    private List<Lesson> lessons;
}
