package com.mts.teta.task2.mapper;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.model.request.CourseRequest;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.response.CourseResponse;
import com.mts.teta.task2.model.response.LessonResponse;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface LessonMapper {
    LessonRequest toDto(Lesson source);
    LessonResponse toResponseDto(Lesson source);
    Lesson toEntity(LessonRequest source);
    void updateLessonFromDto(LessonRequest dto, @MappingTarget Lesson entity);
}
