package com.mts.teta.task2.model.response.exception;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class NotFoundResponse {
    private final String massage;
    private final String dateOccurred;
}
