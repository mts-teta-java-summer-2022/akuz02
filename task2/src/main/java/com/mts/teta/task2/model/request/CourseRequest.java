package com.mts.teta.task2.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
public class CourseRequest {
    private Long id;
    private String author;
    private String title;
    private String category;
}
