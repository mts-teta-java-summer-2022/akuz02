package com.mts.teta.task2.service;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.domain.Lesson;
import com.mts.teta.task2.domain.Role;
import com.mts.teta.task2.domain.User;
import com.mts.teta.task2.mapper.LessonMapper;
import com.mts.teta.task2.mapper.UserMapper;
import com.mts.teta.task2.model.request.LessonRequest;
import com.mts.teta.task2.model.request.UserRequest;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.model.response.UserResponse;
import com.mts.teta.task2.repository.CourseRepository;
import com.mts.teta.task2.repository.LessonRepository;
import com.mts.teta.task2.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper mapper;

    @Override
    public UserResponse createUser(UserRequest userRequest) {
        Optional<User> user = userRepository.findByUsername(userRequest.getUsername());
        User newUser = null;
        if (user.isEmpty()) {
            newUser = new User(
                    userRequest.getId(),
                    userRequest.getUsername(),
                    userRequest.getEmail(),
                    passwordEncoder.encode(userRequest.getPassword()),
                    null,
                    null);
            userRepository.save(
                    newUser
            );
        }
        return mapper.toResponseDto(newUser);
    }

    @Override
    public UserResponse deletedUser(Long userId) {
        return null;
    }
}
