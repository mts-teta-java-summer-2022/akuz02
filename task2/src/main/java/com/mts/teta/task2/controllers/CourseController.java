package com.mts.teta.task2.controllers;

import com.mts.teta.task2.domain.Course;
import com.mts.teta.task2.mapper.CourseMapper;
import com.mts.teta.task2.model.request.CourseRequest;
import com.mts.teta.task2.model.response.CourseResponse;
import com.mts.teta.task2.model.response.LessonResponse;
import com.mts.teta.task2.service.CourseService;
import com.mts.teta.task2.service.LessonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(value = "course-docs")
@RestController
@RequestMapping("/api/v1/course")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;
    private final CourseMapper mapper;

    @ApiOperation(value = "The operation of filtering courses by title and author")
    @GetMapping(path = "/filter")
    public ResponseEntity<CourseResponse> getCoursesByTitleAndAuthor(@RequestParam(value = "title") String title, @RequestParam(value = "author") String author) {
        if (courseService.getCourseByAuthorAndTitle(title, author).isPresent()) {
            return ResponseEntity.of(courseService.getCourseByAuthorAndTitle(title, author).map(mapper::toDto));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @ApiOperation(value = "The operation of selecting courses by prefix")
    @GetMapping(path = "/filterByPrefix")
    public ResponseEntity<List<CourseResponse>> getCoursesByTitlePrefix(@RequestParam(value = "title") String title) {
        List<CourseResponse> body = courseService.getCourseByPrefix(title)
                .stream()
                .map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<List<CourseResponse>>(body, HttpStatus.OK);
    }

    @ApiOperation(value = "Course creation operation")
    @PostMapping("/create")
    public ResponseEntity<Course> createCourse(@Valid @RequestBody CourseRequest request) {
        Course course = courseService.createCourse(request);
        if (course != null) {
            return ResponseEntity.ok(course);
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @ApiOperation(value = "Operation of deleting courses by ID")
    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteCourse(@RequestParam(name = "id") String id) {
        if (courseService.deleteCourse(Integer.parseInt(id))) {
            return ResponseEntity.ok().body("Course was deleted");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @ApiOperation(value = "Operation of updating courses by ID")
    @PutMapping("/update")
    public ResponseEntity<Course> updateCourse(@Valid @RequestBody CourseRequest request, @RequestParam(name = "id") String id) {
        Course course = courseService.updateCourse(Integer.parseInt(id), request);
        if (course != null) {
            return ResponseEntity.ok(course);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{courseId}/assign")
    public ResponseEntity<Course> assignUser(@PathVariable("courseId") Long courseId,
                                             @RequestParam("userId") Long id) {
        Course course = courseService.assignUser(courseId, id);
        return ResponseEntity.ok(course);
    }
}